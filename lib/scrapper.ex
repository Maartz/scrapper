defmodule Scrapper do
  def work() do
    # fake processing delay
    1..5
    |> Enum.random()
    |> :timer.seconds()
    |> Process.sleep()
  end
end
